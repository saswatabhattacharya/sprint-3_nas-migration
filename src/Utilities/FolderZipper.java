package Utilities;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;

public class FolderZipper {
	

	
	public static String copySrcFolderToDestFolder(String ScFolderPathUFT,String TCFolder)  {
	
	String finalRes=null;;
	
	String src=ScFolderPathUFT.replace("\\", "/");
	String dest=TCFolder.replace("\\", "/");

	String latestFileNameSRC=FolderZipper.getLatestFileFromDir(src);

	System.out.println("src Latest file path  "+src+"/"+latestFileNameSRC);
	System.out.println("src Dest file path "+dest+"/"+latestFileNameSRC);

	File copied = new File(dest+"/"+latestFileNameSRC);
	File original = new File(src+"/"+latestFileNameSRC);	   

	try{
	
	FileUtils.copyFile(original, copied);

	System.out.println("Latest file "+latestFileNameSRC);
	
	finalRes=latestFileNameSRC;
	
    }

   catch(Exception e){
	
	   finalRes=null;
	
     }
	
	
	finally{	
		return finalRes;
	}
	
	
	
	}	
	
	
	
	
	
	  
	 public static String getLatestFileFromDir(String dirPath) {
		  
		 //File dir = new File("C:\\Appworx_Job_Run\\ResultpathScreenshot\\TC_017");
		 File dir = new File(dirPath);
		 String latest=null;
		  
	  
		 File[] files = dir.listFiles();
	         Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
	         for (File file : files) {     
	               System.out.println("File: " +file.getName());
	               
	               latest=file.getName();
	               break;        
	         }
	       
	      return latest;  
	         
	  
	  }
	
	
	
	
	
	  public static void main(String[] a) throws Exception {
	    zipFolder("S:\\AutomationResultsSelenium\\Result_06-10-2017-20-43-40", "S:\\AutomationResultsSelenium\\Result_06-10-2017-20-43-40.zip");
	    
	    
	    
	  }

	  static public void zipFolder(String srcFolder, String destZipFile) throws Exception {
	    ZipOutputStream zip = null;
	    FileOutputStream fileWriter = null;

	    fileWriter = new FileOutputStream(destZipFile);
	    zip = new ZipOutputStream(fileWriter);

	    addFolderToZip("", srcFolder, zip);
	    zip.flush();
	    zip.close();
	  }

	  static private void addFileToZip(String path, String srcFile, ZipOutputStream zip)
	      throws Exception {

	    File folder = new File(srcFile);
	    if (folder.isDirectory()) {
	      addFolderToZip(path, srcFile, zip);
	    } else {
	      byte[] buf = new byte[1024];
	      int len;
	      FileInputStream in = new FileInputStream(srcFile);
	      zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
	      while ((len = in.read(buf)) > 0) {
	        zip.write(buf, 0, len);
	      }
	      in.close();
	    }
	  }

	  static private void addFolderToZip(String path, String srcFolder, ZipOutputStream zip)
	      throws Exception {
	    File folder = new File(srcFolder);

	    for (String fileName : folder.list()) {
	      if (path.equals("")) {
	        addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip);
	      } else {
	        addFileToZip(path + "/" + folder.getName(), srcFolder + "/" + fileName, zip);
	      }
	    }
	  }
	  public static boolean copyfile(String source, String dest){
		  boolean res=false;
		  try{
			  
			  Path sourceDirectory = Paths.get(source);
		      Path targetDirectory = Paths.get(dest);

		      //copy source to target using Files Class
		      Files.copy(sourceDirectory, targetDirectory);
		      res=true;
			  return res;
		  }
		  
		  catch(Exception e){
			  res=false;
			  return res;
		  }
		  
	  }
	  
	  
	  
	  
	 public static boolean purgeDirectory(File dir) {
		  boolean purgefiles=false;
		  try{
			  for (File file: dir.listFiles()) {
			        if (file.isDirectory())
			            purgeDirectory(file);
			        file.delete();
			        purgefiles=true;
			    }
			}catch(Exception e){
				purgefiles=true;
			}
		  return purgefiles;
		  }
	

	 public static String copySrcFolderDocFileToDestFolder(String ScFolderPathUFT,String TCFolder)  {
			
			String finalRes=null;;
			
			String src=ScFolderPathUFT.replace("\\", "/");
			String dest=TCFolder.replace("\\", "/");

			String latestFileNameSRC=FolderZipper.getLatestDocFileFromDir(src);

			System.out.println("src Latest file path  "+src+"/"+latestFileNameSRC);
			System.out.println("src Dest file path "+dest+"/"+latestFileNameSRC);

			File copied = new File(dest+"/"+latestFileNameSRC);
			File original = new File(src+"/"+latestFileNameSRC);	   

			try{
			
			FileUtils.copyFile(original, copied);

			System.out.println("Latest file "+latestFileNameSRC);
			
			finalRes=latestFileNameSRC;
			
		    }

		   catch(Exception e){
			
			   finalRes=null;
			
		     }
			
			
			finally{	
				return finalRes;
			}
			
			
			
			}
	 
	 
	 
	 
	 public static String copySrcFolderHtmlReportToDestFolder(String ScFolderPathUFT,String TCFolder)  {
			
			String finalRes=null;;
			
			String src=ScFolderPathUFT.replace("\\", "/");
			String dest=TCFolder.replace("\\", "/");

			String latestFileNameSRC=FolderZipper.getLatestUFTHTMLReport(src);

			System.out.println("src Latest file path  "+src+"/"+latestFileNameSRC);
			System.out.println("src Dest file path "+dest+"/"+latestFileNameSRC);

			File copied = new File(dest+"/"+latestFileNameSRC);
			File original = new File(src+"/"+latestFileNameSRC);	   

			try{
			
			FileUtils.copyFile(original, copied);

			System.out.println("Latest file "+latestFileNameSRC);
			
			finalRes=latestFileNameSRC;
			
		    }

		   catch(Exception e){
			
			   finalRes=null;
			
		     }
			
			
			finally{	
				return finalRes;
			}
			
			
			
			}
	 
	 
	 public static String lastFileModified(String directorypath) {
		 File dir = new File(directorypath);
		 File [] files = dir.listFiles(new FilenameFilter() {
		     @Override
		     public boolean accept(File dir, String name) {
		         return name.endsWith(".dat");
		     }
		 });
		 long lastMod = Long.MIN_VALUE;
		 String choice = null;
		 for (File file : files) {
		 if (file.lastModified() > lastMod) {
		 choice = file.getName();
		 lastMod = file.lastModified();
		 }
		    }
		 return choice;
		 }
	 public static String lastFileModifiedGZ(String directorypath) {
		 File dir = new File(directorypath);
		 File [] files = dir.listFiles(new FilenameFilter() {
		     @Override
		     public boolean accept(File dir, String name) {
		         return name.endsWith(".gz");
		     }
		 });
		 long lastMod = Long.MIN_VALUE;
		 String choice = null;
		 for (File file : files) {
		 if (file.lastModified() > lastMod) {
		 choice = file.getName();
		 lastMod = file.lastModified();
		 }
		    }
		 return choice;
		 }
	 public static String lastFileModifiedCSV(String directorypath) {
		 File dir = new File(directorypath);
		 File [] files = dir.listFiles(new FilenameFilter() {
		     @Override
		     public boolean accept(File dir, String name) {
		         return name.endsWith(".csv");
		     }
		 });
		 long lastMod = Long.MIN_VALUE;
		 String choice = null;
		 for (File file : files) {
		 if (file.lastModified() > lastMod) {
		 choice = file.getName();
		 lastMod = file.lastModified();
		 }
		    }
		 return choice;
		 }
 
	 
	 public static String getLatestDocFileFromDir(String dirPath) {
		  
		 //File dir = new File("C:\\Appworx_Job_Run\\ResultpathScreenshot\\TC_017");
		 File dir = new File(dirPath);
		 String latest=null;
		  
	  System.out.println("dirPath:"+dirPath);
		 File[] files = dir.listFiles();
		 System.out.println("Files list:"+files);
	         Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
	         for (File file : files) {     
	               System.out.println("File: " +file.getName());
	               
	               latest=file.getName();
	         
	               if (latest.endsWith(".doc")){
	               
	            	   break; 
	               }
	               else{
	            	   continue;
	               }
	                     
	         }
	       
	      return latest;  
	         
	  
	  }
	 

	 public static String lastCSVFileModified(String directorypath) {
		 File dir = new File(directorypath);
		 File [] files = dir.listFiles(new FilenameFilter() {
		     @Override
		     public boolean accept(File dir, String name) {
		         return name.endsWith(".csv");
		     }
		 });
		 long lastMod = Long.MIN_VALUE;
		 String choice = null;
		 for (File file : files) {
		 if (file.lastModified() > lastMod) {
		 choice = file.getName();
		 lastMod = file.lastModified();
		 }
		    }
		 return choice;
		 }
	 
	 public static ArrayList<String> listAllFiles(String directorypath)
	 {
		 File folder = new File(directorypath);
		 File[] listOfFiles = folder.listFiles();
		 ArrayList <String> fileNames =new ArrayList<String>();

		 for (int i = 0; i < listOfFiles.length; i++) {
		   if (listOfFiles[i].isFile()) {
		     System.out.println("File " + listOfFiles[i].getName());
		     fileNames.add(listOfFiles[i].getName());
		   } else if (listOfFiles[i].isDirectory()) {
		     System.out.println("Directory " + listOfFiles[i].getName());
		   }
		 }
		 return fileNames;
	 }
	 
	 
	 
	 public static String getLatestUFTHTMLReport(String dirPath) {
		  
		 //File dir = new File("C:\\Appworx_Job_Run\\ResultpathScreenshot\\TC_017");
		 File dir = new File(dirPath);
		 String latest=null;
		  
	  System.out.println("dirPath:"+dirPath);
		 File[] files = dir.listFiles();
		 System.out.println("Files list:"+files);
	         Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
	         for (File file : files) {     
	               System.out.println("File: " +file.getName());
	               
	               latest=file.getName();
	         
	               if (latest.startsWith("Execution")){
	               
	            	   break; 
	               }
	               else{
	            	   continue;
	               }
	                     
	         }
	       
	      return latest;  
	         
	  
	  }
	 
	}