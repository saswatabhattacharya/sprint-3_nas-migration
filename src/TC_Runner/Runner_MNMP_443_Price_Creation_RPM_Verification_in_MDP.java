package TC_Runner;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import Utilities.GetTCDetailsFromDataSheet;
import Utilities.Reporting_Utilities;
import Utilities.getCurrentDate;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Runner_MNMP_443_Price_Creation_RPM_Verification_in_MDP {

	@Test
	public void test_ItemCreation() throws IOException {

		ExtentReports extent=null;
		ExtentTest logger=null;
		String ResultPath=null;
		XWPFRun xwpfRun=null;
		XWPFDocument doc=null;
		
		String Final_Result="FAIL";
		
		
		String  TestKeyword = "Price_flow_from_RPM_to_MDP";
		
		

		String TestCaseNo=GetTCDetailsFromDataSheet.TCDetails(TestKeyword).get("TestCaseNo");
		String TestCaseName=GetTCDetailsFromDataSheet.TCDetails(TestKeyword).get("TestCaseName");


		if(ResultPath==null){

			String ResPath=System.getProperty("user.dir") +"/Results"+"/"+"MNMP_443_Price_Creation_"+getCurrentDate.getISTDateddMM();

			ResultPath=Reporting_Utilities.createResultPath(ResPath);	
			//ResultPath=Reporting_Utilities.createResultPath("NAS "+getCurrentDate.getISTDateddMM()+"/run_RMSE_RPAS_ITEM_MASTER");

			ResultPath.replace("\\", "/");
			System.setProperty("resultpath",ResultPath);
		}

		System.out.println("Result path is "+ResultPath);


//		String TC_ScFolder=ResultPath+"/"+TestCaseNo;
		String TC_ScFolder=ResultPath+TestCaseNo;

		System.out.println("Sc result path is "+TC_ScFolder);


		File dir = new File(TC_ScFolder);

		if(!dir.exists()){

			dir.mkdir();

		}

		try{

			
			doc = new XWPFDocument();
	        XWPFParagraph p = doc.createParagraph();
	        xwpfRun = p.createRun();  
			
			
			extent = new ExtentReports (ResultPath+"RPM_PriceCreation_SeleniumReport.html", false);
			extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
			//		 //*******************************************************************************************************
			//		//****************************Instantiate Extent Reporting************************
			logger=extent.startTest(TestCaseNo+":"+TestCaseName);



//			Location_RMS_To_Relex.TC_017_Step1_ExecuteJob_NB_RMSE_BLUEYONDER_LOCS.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			RPM_Price_Creation_verification_PriceIntegrity_In_MDP.TC_Step1_Execute_RPM_RegularPriceCreation_UFT.Prepare(extent, logger,ResultPath,TC_ScFolder,xwpfRun);
			
			Result ResStep1 = JUnitCore.runClasses(RPM_Price_Creation_verification_PriceIntegrity_In_MDP.TC_Step1_Execute_RPM_RegularPriceCreation_UFT.class);
			System.out.println("Result Step:"+ResStep1);

			boolean step1= ResStep1.wasSuccessful();
			System.out.println("Run Step:"+step1);
			
			
			if(step1)
			//if(step1&&step2&&step3&&step4&&step5&&step6&&step7)
			
		{
				
				 Final_Result="PASS";
					logger.log(LogStatus.PASS, "TestCaseNo::"+TestCaseName+":: PASS"); 
					
					Assert.assertTrue(TestCaseNo+"--"+TestCaseName,true);
					
					
				}

				else {

					logger.log(LogStatus.FAIL, "TestCaseNo::"+TestCaseName+":: FAIL"); 
					Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);

				}

		}


		catch(Exception e){

			System.out.println("The exception is "+e);

		}


		finally{

			extent.endTest(logger);
			extent.flush();
			
			FileOutputStream out1;
		
				out1 = new FileOutputStream(TC_ScFolder+"/"+"RPM_Regular_Price_Creation_"+Final_Result+".docx");
				doc.write(out1);
				out1.close();
				doc.close();

		}



	}

}
