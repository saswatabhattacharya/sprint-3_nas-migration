package ItemLocation_RMS_To_Retalix;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;























import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Functions.McColls_Amazon_RackSpace;
import Functions.NFSI_PostCall;
import Functions.StockAdjustmentDB_JDBC;
import Functions.WinSCPCall;
import Utilities.*;



public class Step1_Item_Location_RMS_to_Retalix_insertDataNB_RANGE {


	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	public static String  ResultPath=null;
	public static String  TCFolder=null;
	public static XWPFRun xwpfRun=null;
	

	
	
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String Final_Result="";
	String TestDataPath;

	Integer stepnum=0;
		

	
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	
	
	public static void Prepare(ExtentReports extent1,ExtentTest logger1,String ResultPath1,String TCFolder1,XWPFRun xwpf1){

		extent=extent1;
		logger=logger1;
		ResultPath=ResultPath1;
		TCFolder=TCFolder1;
		xwpfRun=xwpf1;
	}


	public static String getResultPath()
	{
       return ResultPath;

	}


	public static String getTCFolderPath()
	{
        return TCFolder;

	}



	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {
		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");
	}
	//*******************************************************************************************************
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {


	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {


	}


	@Before	
	public void setUp() throws Exception {

		DriverPath=ProjectConfigurations.LoadProperties("DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("DriverName");
		DriverType=ProjectConfigurations.LoadProperties("DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");							
		TestDataPath=ProjectConfigurations.LoadProperties("TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("SheetName");

	}

	@After
	public void tearDown() throws Exception {
		

	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "Item_Location_Flow_RMS_to_Retalix";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;		
		
		String dbUserName=null;
		String dbPassword=null;
		String dbHost=null;
		String dbPort=null;
		String dbService=null;
		
		String ItemID=null;
		String ItemDesc=null;
		String Location=null;
		
		String VDate=null;
		
	

		String consolidatedScreenshotpath="";
		//-------------------------------------------------

		int r = 0;
		try {

			//-----------------------------------------------
			int rows = 0;
			int occurances = 0;

			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}

			
			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
				System.out.println("Keyword: "+Keyword);
				System.out.println("TestKeyword: "+TestKeyword);
				
				if(occurances>0){
					break;
				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {

					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
				
					dbUserName=sheet1.getCell(map.get("dbUserName"), r).getContents().trim();
					dbPassword=sheet1.getCell(map.get("dbPassword"), r).getContents().trim();
					dbHost=sheet1.getCell(map.get("dbHost"), r).getContents().trim();
					dbPort=sheet1.getCell(map.get("dbPort"), r).getContents().trim();
					dbService=sheet1.getCell(map.get("dbService"), r).getContents().trim();
					ItemID=sheet1.getCell(map.get("ItemID"), r).getContents().trim();
					ItemDesc=sheet1.getCell(map.get("ItemDesc"), r).getContents().trim();
					Location=sheet1.getCell(map.get("Location"), r).getContents().trim();
					
					VDate=sheet1.getCell(map.get("VDate"), r).getContents().trim();
					
					consolidatedScreenshotpath=TCFolder;

					
			boolean insertITEMLOC=RMS_DB.insertIntoNB_RANGETable(ItemID, ItemDesc,Location,VDate, dbHost,dbPort, dbUserName, dbPassword, dbService, TestCaseNo,ResultPath,xwpfRun,logger);
			//
									
			if(insertITEMLOC) {

				System.out.println("Item Location Ranging is done properly for Item - "+ItemID+" and Location - "+Location);

				excelCellValueWrite.writeValueToCell("PASS", r, map.get("Result"), DriverSheetPath, SheetName);

				logger.log(LogStatus.PASS, "Item Location Ranging is done properly for Item - "+ItemID+" and Location - "+Location);
			}
			else{
				
			
				logger.log(LogStatus.FAIL, " Item Location Ranging is failed");
				
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");

			}
			
			String afterInsertValidation=RMS_DB.ValidateNB_RANGETable(ItemID,Location,dbHost,dbPort, dbUserName, dbPassword, dbService, TestCaseNo,ResultPath,xwpfRun,logger);
					
			if(afterInsertValidation.equalsIgnoreCase(ItemID)){

					System.out.println("Item - "+ItemID+"  is present in NB_RANGE table");

					excelCellValueWrite.writeValueToCell("PASS", r, map.get("Result"), DriverSheetPath, SheetName);

					logger.log(LogStatus.PASS, "Item - "+ItemID+" and Loc - "+Location+"  is present in NB_RANGE table");
					}
					
			else{
						
						
					logger.log(LogStatus.FAIL, "Item - "+ItemID+" and Loc - "+Location+" not  present in the NB_RANGE table");
						
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");

					}		
					
			boolean beforeJobDBVal=RMS_DB.VerifyTableifEmptyRMS(ItemID,Location,dbHost,dbPort, dbUserName, dbPassword, dbService, TestCaseNo,ResultPath,xwpfRun,logger);
			
			if(beforeJobDBVal) {

				System.out.println("BeforeJob : Item is not present in ITEM_LOC table");

				excelCellValueWrite.writeValueToCell("PASS", r, map.get("Result"), DriverSheetPath, SheetName);

				logger.log(LogStatus.PASS, "Before NB_GAL_RANGE Job : Item - "+ItemID+" and Loc - "+Location+" is not present in ITEM_LOC table "+ItemID+"   "+Location);
			}
			else{
				
				
				logger.log(LogStatus.FAIL, "Before NB_GAL_RANGE Job : Item - "+ItemID+" and Loc - "+Location+"  is present in the ITEM_LOC table");
				
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");

				}
			
			
			}

		}
		}

		catch (Exception e) {

			e.printStackTrace();
			Final_Result="FAIL"; 
			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			
			Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
			
			
		}
		finally
		{  
			
			
		
			

		}
	}

}

