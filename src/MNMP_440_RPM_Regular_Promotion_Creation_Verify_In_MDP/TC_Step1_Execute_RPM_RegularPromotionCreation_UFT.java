package MNMP_440_RPM_Regular_Promotion_Creation_Verify_In_MDP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.*;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import Utilities.GetTCDetailsFromDataSheet;
import Functions_Appworx.*;
import Utilities.FolderZipper;
import Utilities.MyException;
import Utilities.ProjectConfigurations;
import Utilities.Reporting_Utilities;
import Utilities.RowGenerator;
import Utilities.excelCellValueWrite;
import Utilities.getCurrentDate;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class TC_Step1_Execute_RPM_RegularPromotionCreation_UFT {

	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	public static String  ResultPath=null;
	public static String  TCFolder=null;
	 public static XWPFRun xwpfRun=null;

	static String  TestKeyword = "Promotion_flow_from_RPM_to_MDP";	
	static String TestCaseNo=null;
	static String TestCaseName=null;
	
	String script_Location="C:/UFT_Scripts_Exec_Source/RPM_Promotion_Creation/RPM_Promotion_CreationVB.vbs";
	
	String scriptStatus="C:/UFT_Scripts_Exec_Source/RPM_Promotion_Creation/";
	

	String ResultPathUFT="C:/UFT_Scripts_Exec_Source/RPM_Promotion_Creation/Resultpath/";
	
	
//	String script_Location="S:/Morrisons Automation(UFT)/NAS & DOCKER UPGRADE/UFT_Scripts_Exec_Source/RPM_Promotion_Creation/RPM_Promotion_CreationVB.vbs";
	String UFTscriptStatus="C:/UFT_Scripts_Exec_Source/RPM_Promotion_Creation/";
//	String ResultPathUFT="S:/Morrisons Automation(UFT)/NAS & DOCKER UPGRADE/UFT_Scripts_Exec_Source/RPM_Promotion_Creation/Resultpath";
//	String ResultPathUFT=null;
	
	
	Integer stepnum=0;
	

	public static void Prepare(ExtentReports extent1,ExtentTest logger1,String ResultPath1,String TCFolder1,XWPFRun xwpf1){

		extent=extent1;
		logger=logger1;
		ResultPath=ResultPath1;
		TCFolder=TCFolder1;
		xwpfRun=xwpf1;
	}


	public static String getResultPath()
	{
		return ResultPath;

	}


	public static String getTCFolderPath()
	{
		return TCFolder;

	}

	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {

		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");

		TestCaseNo=GetTCDetailsFromDataSheet.TCDetails(TestKeyword).get("TestCaseNo");
		TestCaseName=GetTCDetailsFromDataSheet.TCDetails(TestKeyword).get("TestCaseName");
	

	}
	//*******************************************************************************************************



	@After
	public void tearDown() throws Exception {


	}

	@Test
	public void test() throws IOException {

		//UFTscriptStatus=ProjectConfigurations.LoadProperties("UFTscriptStatus");
//		UFTscriptStatus=ProjectConfigurations.LoadProperties("UFTscriptStatus");
		
		
		try{	

			
				
		
			System.out.println("Result:"+ResultPath);
		System.out.println("Before UFT function call ------TC no param1 "+TestCaseNo);
			System.out.println("TC no param2 "+ResultPath);
			System.out.println("UFTscriptStatus:"+UFTscriptStatus);
			
			Boolean res=BatchJoB_NFSI.RunUFT_Script_Promo(TestCaseNo,ResultPathUFT,script_Location,UFTscriptStatus);
			
			
			System.out.println("Return result :"+res);

			if(res){

				logger.log(LogStatus.PASS, "UFT Script Execution for :"+TestKeyword+" Successful");

				Assert.assertTrue("Execute UFT Execution ",true);

			}			

			else{

				logger.log(LogStatus.FAIL, "UFT Script Execution for :"+TestKeyword+" UnSuccessful");

				Assert.assertTrue("Execute UFT Execution ",false);

				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			}
		}



		catch (Exception e) {

			e.printStackTrace();
			Assert.assertTrue("Execute UFT Execution Script ",false);

		}


		finally

		{  System.out.println("Finally started");

			String dest=getTCFolderPath().replace("\\","/");
			
			System.out.println("Destination folder path is "+dest);
			
			String UFTResults= BatchJoB_NFSI.UFTResultsPath(UFTscriptStatus);
			
			String  LatestScFile=FolderZipper.copySrcFolderDocFileToDestFolder(UFTResults, getTCFolderPath());

			System.out.println("Latest file  is "+dest);
			
			String latestHtmlReport=FolderZipper.copySrcFolderHtmlReportToDestFolder(UFTResults, getTCFolderPath());
			
			if(latestHtmlReport!=null){
				logger.log(LogStatus.INFO, "Link for UFT HTML report <a href='file:///"+dest+"/"+latestHtmlReport+"'>       Click here to view HTML report   !!!</a>");
			}
			
			System.out.println("The UFT HTML Report has copied successfully");
						
						
//			
		if(LatestScFile!=null){
				
			
			logger.log(LogStatus.INFO, "Evidence link for UFT Execution <a href='file:///"+dest+"/"+LatestScFile+"'>     EvidenceLink</a>");

		}
			


		}


	}}


