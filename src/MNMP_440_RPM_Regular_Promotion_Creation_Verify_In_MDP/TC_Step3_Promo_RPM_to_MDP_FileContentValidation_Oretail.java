package MNMP_440_RPM_Regular_Promotion_Creation_Verify_In_MDP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Functions.McColls_Amazon_RackSpace;
import Functions.NFSI_PostCall;
import Functions.WinSCPCall;
import Utilities.*;

public class TC_Step3_Promo_RPM_to_MDP_FileContentValidation_Oretail {

	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	public static String  ResultPath=null;
	public static String  TCFolder=null;
	public static XWPFRun xwpfRun=null;

	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String Final_Result="";
	String Downloadfilepath="";
	String TestDataPath;

	String DownloadFolderPath_Oretail="";
	String DownloadFolderPath_NAS="";

	String OretailFileName="";
	String NasFileName="";

	public WebDriver driver = null;

	public WebDriverWait wait = null;

	public static void Prepare(ExtentReports extent1,ExtentTest logger1,String ResultPath1,String TCFolder1,XWPFRun xwpf1){

		extent=extent1;
		logger=logger1;
		ResultPath=ResultPath1;
		TCFolder=TCFolder1;
		xwpfRun=xwpf1;
	}


	public static String getResultPath()
	{
		return ResultPath;

	}


	public static String getTCFolderPath()
	{
		return TCFolder;

	}





	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {
		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");
	}
	//*******************************************************************************************************
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {


		DriverPath=ProjectConfigurations.LoadProperties("DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("DriverName");
		DriverType=ProjectConfigurations.LoadProperties("DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");			
		TestDataPath=ProjectConfigurations.LoadProperties("TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("SheetName");	

		ResultPath=getResultPath();


	}

	@After
	public void tearDown() throws Exception {


	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "Promotion_flow_from_RPM_to_MDP";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;		
		String consolidatedScreenshotpath="";
		String DownloadFolderPath_Oretail="";
		

		String OretailFileName="";
		String NasFileName="";
		String UnixHostname=null;
		String UnixPort=null;
		String UnixUsername=null;
		String UnixPassword=null;
		String NasPath=null;
		String OretailPath=null;
		String filePrefix=null;
		String NAS_DownloadPath=null;
		String PromoID=null;
		
		
		int r = 0;

		try {


			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}


			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
				System.out.println("Keyword: "+Keyword);
				System.out.println("TestKeyword: "+TestKeyword);


				if(occurances>0){
					break;
				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {

					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();				
					DownloadFolderPath_Oretail=sheet1.getCell(map.get("DownloadFolderPath_Oretail"), r).getContents().trim();
	
					
					UnixHostname=sheet1.getCell(map.get("UnixHostname"), r).getContents().trim();
					UnixPort=sheet1.getCell(map.get("UnixPort"), r).getContents().trim();
					UnixUsername=sheet1.getCell(map.get("UnixUsername"), r).getContents().trim();
					UnixPassword=sheet1.getCell(map.get("UnixPassword"), r).getContents().trim();
					
					OretailPath=sheet1.getCell(map.get("OretailPath"), r).getContents().trim();


					filePrefix=sheet1.getCell(map.get("filePrefix"), r).getContents().trim();	

					PromoID=sheet1.getCell(map.get("PromotionID"), r).getContents().trim();

				

					consolidatedScreenshotpath=getTCFolderPath()+"/";

					//************************** Oretail latest file extract ****************************************
					
					System.out.println("Oretail Path is : "+OretailPath);
					
					String OretailFile = WinSCPCall.oretailLatestMDPfileExtract(UnixUsername, UnixPassword, OretailPath,filePrefix,UnixHostname,UnixPort,TestCaseNo,ResultPath,xwpfRun, logger);

					//
											
					if(OretailFile!=null) {

								System.out.println(OretailFile);

								excelCellValueWrite.writeValueToCell(OretailFile, r, map.get("OretailFileName"), DriverSheetPath, SheetName);

								logger.log(LogStatus.PASS, "Extract File presence at Oretail Path "+OretailPath+" validated successfully ");
							}
							else{
								
								logger.log(LogStatus.FAIL, " file presence validation in Oretail path failed");
								
								throw new MyException("Test Stopped Because of Failure. Please check Execution log");

							}
			
					OretailFileName=sheet1.getCell(map.get("OretailFileName"), r).getContents().trim();
					
					
							
				           //*************************** File Download***************************//
					
						System.out.println("Download file path -> "+DownloadFolderPath_Oretail);				
							
						Boolean downloadFile = WinSCPCall.DownloadFileFrom_Oretail(UnixUsername, UnixPassword,OretailFileName,DownloadFolderPath_Oretail, OretailPath,UnixHostname,UnixPort,TestCaseNo,ResultPath,xwpfRun, logger);

							if(downloadFile==true) {
														
						System.out.println(downloadFile);

								
								logger.log(LogStatus.PASS, "File downloaded successfully from Oretail path: "+OretailPath+" to Path: "+DownloadFolderPath_Oretail+"");

							}
							
							
							else{
								logger.log(LogStatus.FAIL, "File download failed");
								throw new MyException("Test Stopped Because of Failure. Please check Execution log");

							}
						
							
						
					//*********************PromoID validation in Extract File******************************

					boolean PromoIDValidation = WinSCPCall.ValidatePromoId(DownloadFolderPath_Oretail, OretailFileName, PromoID,TestCaseNo,ResultPath,xwpfRun, logger);


					if(PromoIDValidation==true) {

						Final_Result="PASS";
						logger.log(LogStatus.PASS, " Promotion ID -> "+PromoID+"  is present in the extracted file ");					
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);

					}


					else{


						logger.log(LogStatus.FAIL, " Promotion ID - >"+PromoID+"  is not present in the extracted file ");

						Final_Result="FAIL";

						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);

						throw new MyException("Test Stopped Because of Failure. Please check Execution log");					
					}
				}


			}

		}


		catch (Exception e) {

			e.printStackTrace();

			Final_Result="FAIL"; 

			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
		}

		finally

		{  



		}
	}

}

